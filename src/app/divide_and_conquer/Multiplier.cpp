#include "app/divide_and_conquer/Multiplier.hpp"

#include <algorithm>
#include <vector>

namespace divide_and_conquer
{
Multiplier::Multiplier(const std::string& x, const std::string& y) : first(x), second(y)
{
}

std::string Multiplier::school()
{
    std::vector<std::string> partialResults;
    for (auto i = second.size(); i > 0; --i)
    {
        auto result = multiplyByDigit(first, second.at(i - 1));
        result = multiplyByPowerOfTen(result, second.size() - i);
        partialResults.push_back(result);
    }

    return sumNumbers(partialResults);
}

std::string Multiplier::recursive()
{
    return recursiveImpl(first, second);
}

std::string Multiplier::multiplyByDigit(const std::string& number, char charMultiplierNumber)
{
    int carry = 0;
    std::string sum = "";
    auto multiplier = charMultiplierNumber - '0';
    for (int i = number.size() - 1; i >= 0; --i)
    {
        auto singleNum = multiplier * (number.at(i) - '0');
        if (carry > 0)
        {
            singleNum += carry;
            carry = 0;
        }

        if (singleNum >= 10)
        {
            carry = singleNum / 10;
            singleNum = singleNum % 10;
        }

        sum.push_back(singleNum + '0');
    }
    if (carry > 0)
    {
        sum.push_back(carry + '0');
    }

    std::reverse(sum.begin(), sum.end());
    return sum;
}

std::string Multiplier::multiplyByPowerOfTen(std::string& number, const size_t power)
{
    for (size_t i = 0; i < power; i++)
    {
        number += "0";
    }
    return number;
}
std::vector<std::string> Multiplier::addLeadingZeros(std::vector<std::string>& numbers)
{
    const auto num = std::max_element(
        std::begin(numbers), std::end(numbers), [](std::string& a, std::string& b) { return a.size() < b.size(); })
                         ->size();

    for (auto& number : numbers)
    {
        auto currSize = number.size();
        for (size_t i = 0; i < num - currSize; ++i)
        {
            number = "0" + number;
        }
    }
    return numbers;
}

std::string Multiplier::sumNumbers(std::vector<std::string>& numbers)
{
    numbers = addLeadingZeros(numbers);
    int carry = 0;
    std::string result = "";
    for (size_t i = numbers.begin()->size(); i > 0; --i)
    {
        int singleNum = carry;
        carry = 0;
        for (size_t j = 0; j < numbers.size(); ++j)
        {
            singleNum += (numbers[j][i - 1] - '0');
        }

        if (singleNum >= 10)
        {
            carry = singleNum / 10;
            singleNum = singleNum % 10;
        }

        result.push_back(singleNum + '0');
    }

    if (carry > 0)
    {
        result.push_back(carry + '0');
    }

    std::reverse(std::begin(result), std::end(result));
    return result;
}

std::string Multiplier::recursiveImpl(const std::string& first, const std::string& second)
{
    if (first.size() < 2)
    {
        return multiplyByDigit(first, second.at(0));
    }
    const auto a = std::string(first.begin(), first.begin() + first.size() / 2);
    const auto b = std::string(first.begin() + first.size() / 2, first.end());
    const auto c = std::string(second.begin(), second.begin() + second.size() / 2);
    const auto d = std::string(second.begin() + second.size() / 2, second.end());

    auto ac = recursiveImpl(a, c);
    auto ad = recursiveImpl(a, d);
    auto bc = recursiveImpl(b, c);
    auto bd = recursiveImpl(b, d);

    auto firstRec = multiplyByPowerOfTen(ac, first.size());
    auto secondRec = sumNumbers(std::vector<std::string>{ad, bc});
    secondRec = multiplyByPowerOfTen(secondRec, first.size()/2);

    return sumNumbers(std::vector<std::string>{firstRec, secondRec, bd});
}
}
