#include "app/divide_and_conquer/MinimumCut.hpp"

#include <random>

namespace
{
size_t chooseRandomNumber(size_t maxBound)
{
    // Seed with a real random value, if available
    std::random_device r;

    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(0, maxBound-1);
    return uniform_dist(e1);
}
}
namespace divide_and_conquer
{
int minimumCut(std::map<int, std::list<int>> graph)
{
    if (graph.size() == 2)
    {
        return graph.begin()->second.size();
    }

    auto randomNum1 = chooseRandomNumber(graph.size());
    auto& randomNodeIterator = graph.begin();
    std::advance(randomNodeIterator, randomNum1);
    auto firstNodeId = randomNodeIterator->first;

    auto& firstNodeEdges = randomNodeIterator->second;
    auto randomNum2 = chooseRandomNumber(firstNodeEdges.size());
    auto edgeIterator = std::begin(firstNodeEdges);
    std::advance(edgeIterator, randomNum2);
    auto secondNodeId = *edgeIterator;
    auto secondNodeEdges = graph.at(secondNodeId);


    firstNodeEdges.insert(firstNodeEdges.end(),secondNodeEdges.begin(), secondNodeEdges.end());
    graph.erase(secondNodeId);

    for (auto& node : graph)
    {
        for (auto& x : node.second)
        {
            if (x == secondNodeId)
                x = firstNodeId;
        }
    }

    firstNodeEdges.remove(firstNodeId);

    return minimumCut(graph);
}

}