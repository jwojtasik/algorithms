#pragma once

#include <string>
#include <vector>

namespace divide_and_conquer
{
class Multiplier
{
public:
    Multiplier(const std::string& x, const std::string& y);

    std::string school();
    std::string recursive();

private:
    std::string multiplyByDigit(const std::string& number, char charMultiplierNumber);
    std::string multiplyByPowerOfTen(std::string& number, const size_t power);
    std::vector<std::string> addLeadingZeros(std::vector<std::string>& numbers);
    std::string sumNumbers(std::vector<std::string>& numbers);


    std::string recursiveImpl(const std::string& first, const std::string& second);

    std::string first;
    std::string second;
};
}