#include "app/divide_and_conquer/Sorting.hpp"

#include <iostream>
#include <map>
namespace
{
std::vector<int> merge(std::vector<int> vecA, std::vector<int> vecB, size_t n)
{
    auto i = std::begin(vecA), j = std::begin(vecB);
    std::vector<int> result;

    for (size_t k = 0; k < n; ++k)
    {
        if (i != std::end(vecA) && j != std::end(vecB) && *i < *j)
        {
            result.push_back(*i);
            ++i;
        }
        else if (j != std::end(vecB))
        {
            result.push_back(*j);
            ++j;
        }
        else if (i != std::end(vecA))
        {
            result.push_back(*i);
            ++i;
        }
    }
    return result;
}

void chooseLastElementPivot(std::vector<int>& vec, int left, int right)
{
    std::swap(vec.at(left), vec.at(right));
}

void chooseMedianPivot(std::vector<int>& vec, int left, int right)
{
    std::map<int, int> median;

    median.insert({vec[left], left});
    median.insert({vec[right], right});
    median.insert({vec[left + (right - left) / 2], left + (right - left) / 2});

    auto it = median.begin();
    ++it;
    std::swap(vec.at(it->second), vec.at(left));
}

void printVector(std::vector<int>& vec)
{
    for (const auto& i : vec)
    {
        std::cout << i << ",";
    }
    std::cout << std::endl;
}
}

namespace divide_and_conquer
{
std::vector<int> mergeSort(std::vector<int>& vec)
{
    if (vec.size() == 1)
    {
        return vec;
    }
    auto firstHalf = mergeSort(std::vector<int>{std::begin(vec), std::begin(vec) + vec.size() / 2});
    auto secondHalf = mergeSort(std::vector<int>{std::begin(vec) + vec.size() / 2, std::end(vec)});
    return merge(firstHalf, secondHalf, vec.size());
}

int partition(std::vector<int>& vec, int left, int right)
{
    chooseMedianPivot(vec, left, right);

    auto pivot = vec[left];
    size_t i = left + 1;
    for (size_t j = left + 1; j <= right; ++j)
    {
        if (vec.at(j) < pivot)
        {
            std::swap(vec.at(i), vec.at(j));
            ++i;
        }
    }
    std::swap(vec.at(left), vec.at(i - 1));

    return i - 1;
}

int quickSort(std::vector<int>& vec, int left, int right)
{
    if (left >= right)
        return 0;

    auto comparisonCount = right - left;

    auto pivotPosition = partition(vec, left, right);

    comparisonCount += quickSort(vec, left, pivotPosition - 1);
    comparisonCount += quickSort(vec, pivotPosition + 1, right);

    return comparisonCount;
}

int randomizedSelectionImpl(std::vector<int>& vec, int left, int right, size_t statisticOrder)
{
    if (left >= right)
        return vec.at(left);

    auto i = partition(vec, left, right);

    if (i == statisticOrder - 1)
    {
        return vec.at(i);
    }
    else if (i < statisticOrder - 1)
    {
        return randomizedSelectionImpl(vec, i + 1, right, statisticOrder - i);
    }
    else
    {
        return randomizedSelectionImpl(vec, left, i - 1, statisticOrder);
    }
}

int randomizedSelection(std::vector<int>& vec, size_t statisticOrder)
{
    return randomizedSelectionImpl(vec, 0, vec.size() - 1, statisticOrder);
}
}