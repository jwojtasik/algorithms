#include "app/data_structures/TwoSum.hpp"

#include <set>

namespace data_structures
{
int twoSum(std::unordered_multiset<long long>& us, std::pair<int, int> sumRange)
{
    int numOfPairs = 0;
    std::set<std::pair<long long, long long>> pairs;
    for (int sum = sumRange.first; sum <= sumRange.second; ++sum)
    {
        for (auto num = us.begin(); num != us.end(); ++num)
        {
            auto it = us.find(sum - *num);
            if (it != us.end() && it != num)
            {
                pairs.insert(std::make_pair(*num, *it));
                ++numOfPairs;
                break;
            }
        }
    }

    return pairs.size();
}
}