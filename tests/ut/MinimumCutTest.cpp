#include <fstream>
#include <gmock/gmock.h>
#include <istream>
#include <list>
#include <map>

#include "app/divide_and_conquer/MinimumCut.hpp"

TEST(Graphs, DISABLED_MinimumCutProblem)
{
    std::map<int, std::list<int>> input;
    std::filebuf fb;
    if (fb.open("kargerMinCut.txt", std::ios::in))
    {
        std::istream is(&fb);
        std::string s;
        while (std::getline(is, s))
        {
            std::istringstream iss(s);
            int a;
            iss >> a;
            std::list<int> xd;
            int x;
            while (iss >> x)
            {
                xd.push_back(x);
            }
            input.insert(std::make_pair(a, xd));
        }
        fb.close();
    }

    int x = 205;

    for (int i = 0; i < 20000; ++i)
    {
        auto y = divide_and_conquer::minimumCut(input);
        if (y < x)
            x = y;
    }

    ASSERT_THAT(divide_and_conquer::minimumCut(input), testing::Eq(21));
}

TEST(Graphs, DISABLED_MinimumCut_ShouldFind2Cuts)
{
    std::map<int, std::list<int>> input = {{1, {2, 3, 4, 7}}, {2, {1, 3, 4}}, {3, {1, 2, 4}}, {4, {1, 2, 3, 5}},
        {5, {4, 6, 7, 8}}, {6, {5, 7, 8}}, {7, {1, 5, 6, 8}}, {8, {5, 6, 7}}};

    int x = 205;

    for (int i = 0; i < 1055; ++i)
    {
        auto y = divide_and_conquer::minimumCut(input);
        if (y < x)
            x = y;
    }

    ASSERT_THAT(divide_and_conquer::minimumCut(input), testing::Eq(2));
}

TEST(Graphs, DISABLED_MinimumCut_ShouldFind1Cut)
{
    std::map<int, std::list<int>> input = {{1, {2, 3, 4}}, {2, {1, 3, 4}}, {3, {1, 2, 4}}, {4, {1, 2, 3, 5}},
        {5, {4, 6, 7, 8}}, {6, {5, 7, 8}}, {7, {5, 6, 8}}, {8, {5, 6, 7}}};

    int x = 205;

    for (int i = 0; i < 30055; ++i)
    {
        auto y = divide_and_conquer::minimumCut(input);
        if (y < x)
            x = y;
    }

    ASSERT_THAT(divide_and_conquer::minimumCut(input), testing::Eq(1));
}