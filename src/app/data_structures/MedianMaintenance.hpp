#pragma once

#include <vector>

namespace data_structures
{
float medianOnHeaps(const std::vector<int>& numbers);
}