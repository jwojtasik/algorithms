#pragma once

#include <vector>

namespace divide_and_conquer
{
std::vector<int> mergeSort(std::vector<int>&);
int quickSort(std::vector<int>& vec, int left, int right);
int randomizedSelection(std::vector<int>& vec, size_t statisticOrder);
}