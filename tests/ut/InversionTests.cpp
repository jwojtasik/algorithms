#include <gtest/gtest.h>

#include <istream>
#include <fstream>
#include <vector>


#include "app/divide_and_conquer/InversionCount.hpp"

using namespace testing;
namespace tests
{
TEST(Test, Inversion_ShouldFindFullInversion)
{
    auto x = 28;
    auto result = divide_and_conquer::inversionCount(std::vector<int>{8, 7, 6, 5, 4, 3, 2, 1});

    ASSERT_EQ(x, result.second);
}

TEST(Test, InversionFind_ShouldFind3InverionsInGivenArray)
{
    auto x = 3;


    auto result = divide_and_conquer::inversionCount(std::vector<int>{1, 3, 5, 2, 4, 6});
    ASSERT_EQ(x, result.second);
}

TEST(Test, DISABLED_InversionFind_ShouldFindAllINversionIn100kNArray)
{
    auto x = 2407905288;
    std::vector<int> input;
    std::filebuf fb;
    if (fb.open("input.txt", std::ios::in))
    {
        std::istream is(&fb);
        std::copy(std::istream_iterator<int>(is), std::istream_iterator<int>(), std::back_inserter(input));
        fb.close();
    }

    auto result = divide_and_conquer::inversionCount(input);
    ASSERT_EQ(x, result.second);
}
}