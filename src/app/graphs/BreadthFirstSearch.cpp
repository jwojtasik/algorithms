#include "app/graphs/BreadthFirstSearch.hpp"

#include <queue>
#include <set>
namespace graphs
{
int BFSShortestPath(std::map<int, std::list<int>>& graph, int from, int to)
{
    std::queue<int> que;
    std::map<int, int> nodesToLayers;
    que.push(from);
    nodesToLayers.insert({from, 0});
    int layer = 0;
    while (!que.empty())
    {
        auto& node = graph.at(que.front());

        for (auto& end : node)
        {
            if (nodesToLayers.find(end) == nodesToLayers.end())
            {
                que.push(end);
                nodesToLayers.insert({end, nodesToLayers.at(que.front()) + 1});
            }
        }

        que.pop();
    }

    return nodesToLayers.at(to);
}
int BFSConnectivity(std::map<int, std::list<int>>& graph)
{
    std::set<int> seenNodes;
    int parts = 0;
    for (auto& node : graph)
    {
        if (seenNodes.find(node.first) == seenNodes.end())
        {
            seenNodes.insert(node.first);
            std::queue<int> que;
            que.push(node.first);

            while (!que.empty())
            {
                auto& node2 = graph.at(que.front());

                for (auto& end : node2)
                {
                    if (seenNodes.find(end) == seenNodes.end())
                    {
                        que.push(end);
                        seenNodes.insert(end);
                    }
                }

                que.pop();
            }
            ++parts;
        }
    }
    return parts;
}
}