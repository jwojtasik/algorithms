#include "app/graphs/DepthFirstSearch.hpp"

#include <stack>
#include <set>

namespace graphs
{
    std::set<int> DFSTraverseGraph(std::map<int, std::list<int>>& graph)
    {    
    std::stack<int> stack;
    std::set<int> nodesVisited;
    nodesVisited.insert(graph.begin()->first);
    stack.push(graph.begin()->first);

    int layer = 0;
    while (!stack.empty())
    {
        auto& node = graph.at(stack.top());

        for (auto& end : node)
        {
            if (nodesVisited.find(end) == nodesVisited.end())
            {
                stack.push(end);
                nodesVisited.insert(end);
                break;
            }
            if (end == *node.rbegin())
            {
                stack.pop();
            }
        }
    }

    return nodesVisited;
    }
}