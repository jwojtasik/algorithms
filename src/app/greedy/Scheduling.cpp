#include "app/greedy/Scheduling.hpp"

#include <set>

namespace greedy
{
class subComparator
{
public:
    bool operator()(const std::pair<int, int>& a, const std::pair<int, int>& b)
    {
        auto firstSub = a.first - a.second;
        auto secondSub = b.first - b.second;

        if (firstSub == secondSub)
            return a.first > b.first;

        return (a.first - a.second) > (b.first - b.second);
    };
};

class ratioComparator
{
public:
    bool operator()(const std::pair<int, int>& a, const std::pair<int, int>& b)
    {
        auto firstRatio = a.first / (a.second * 1.f);
        auto secondRatio = b.first / (b.second * 1.f);

        if (firstRatio == secondRatio)
            return a.first > b.first;

        return firstRatio > secondRatio;
    };
};

template <typename T>
long long schedule(std::vector<std::pair<int, int>>& vec)
{
    std::multiset<std::pair<int, int>, T> jobs;
    for (auto& pair : vec)
    {
        jobs.insert(pair);
    }

    long long time = 0;
    long long sum = 0;
    for (auto job : jobs)
    {
        time += job.second;
        sum += time * job.first;
    }

    return sum;
}

long long schedulingSub(std::vector<std::pair<int, int>>& vec)
{
    return schedule<subComparator>(vec);
}
long long schedulingRatio(std::vector<std::pair<int, int>>& vec)
{
    return schedule<ratioComparator>(vec);
}
}