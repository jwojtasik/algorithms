#include <fstream>
#include <iostream>
#include <istream>
#include <set>
#include <sstream>
#include <string>
#include <unordered_set>

#include "app/data_structures/TwoSum.hpp"
#include "app/greedy/Scheduling.hpp"

void twoSum()
{
    std::filebuf fb;
    std::unordered_multiset<long long> us;
    if (fb.open("2sum.txt", std::ios::in))
    {
        std::istream is(&fb);
        std::string s;
        while (std::getline(is, s))
        {
            std::istringstream iss(s);
            long long x;
            while (iss >> x)
            {
                us.insert(x);
            }
        }
        fb.close();
    }

    std::cout << data_structures::twoSum(us, {-10000, 10000});
}

void jobScheduleSubtraction()
{
    std::vector<std::pair<int, int>> vec;

    std::filebuf fb;
    if (fb.open("jobs.txt", std::ios::in))
    {
        std::istream is(&fb);
        std::string s;
        while (std::getline(is, s))
        {
            std::istringstream iss(s);
            std::pair<int, int> x;
            while (iss >> x.first && iss >> x.second)
            {
                vec.push_back(x);
            }
        }
        fb.close();
    }
    auto x = greedy::schedulingSub(vec);
    auto y = greedy::schedulingRatio(vec);
}

int main(int argc, char** argv)
{
    return 0;
}
