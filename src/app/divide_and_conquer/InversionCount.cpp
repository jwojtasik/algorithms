#include "app/divide_and_conquer/InversionCount.hpp"

namespace
{
std::pair<std::vector<int>, long long> sortAndCount(const std::vector<int>& vecA, const std::vector<int>& vecB, size_t n)
{
    long long inversions = 0;
    size_t i =0, j = 0;
    std::vector<int> result;

    for (size_t k = 0; k < n; ++k)
    {
        if (i <vecA.size() && j < vecB.size() && vecA.at(i) < vecB.at(j))
        {
            result.push_back(vecA.at(i));
            ++i;
        }
        else if (j <vecB.size())
        {
            result.push_back(vecB.at(j));
            ++j;
            inversions += vecA.size() - i;
        }
        else if (i < vecA.size())
        {
            result.push_back(vecA.at(i));
            ++i;
        }
    }
    return {result, inversions};
}
}

namespace divide_and_conquer
{
std::pair<std::vector<int>, long long> inversionCount(const std::vector<int>& first)
{
    if (first.size() == 1)
    {
        return make_pair(first, 0);
    }
    auto x = inversionCount(std::vector<int>(std::begin(first), std::begin(first) + first.size() / 2));
    auto y = inversionCount(std::vector<int>(std::begin(first) + first.size() / 2, std::end(first)));

    auto z = sortAndCount(x.first, y.first, first.size());

    return {z.first, x.second + y.second + z.second};
}
}