#pragma once

#include <list>
#include <map>
#include <set>

namespace graphs
{
std::set<int> DFSTraverseGraph(std::map<int, std::list<int>>& graph);
}