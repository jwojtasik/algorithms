#include <gmock/gmock.h>
#include <list>
#include <map>

#include "app/graphs/BreadthFirstSearch.hpp"

//    2 -- 4
//  /       \
// 1      -   6
//  \   /    /
//    3 -- 5
std::map<int, std::list<int>> inputGraph = {{1, {2, 3}}, {2, {1, 4}}, {3, {1, 5, 6}}, {4, {2, 6}},
    {5, {3, 6}}, {6, {4, 5}}};

// 3     9   4      10
// | \ /     |     /
// |  5      |    6
// | /  \    |     \
// 1      7  2      8

std::map<int, std::list<int>> unconnectedGraph = {{1, {3, 5}}, {2, {4}}, {3, {1, 5}}, {4, {2}},
    {5, {1, 3, 7, 9}}, {6, {8, 10}}, {7, {5}}, {8, {6, 10}}, {9, {5}}, {10, {6, 8}}};
using namespace testing;
namespace tests
{
TEST(BreadthFirstSearchTests, BFSPath_SouldBe2From1To4)
{
    auto result = graphs::BFSShortestPath(inputGraph, 1, 4);

    ASSERT_THAT(result, testing::Eq(2));
}
TEST(BreadthFirstSearchTests, BFSPath_SouldBe3From2To5)
{
    auto result = graphs::BFSShortestPath(inputGraph, 2, 5);

    ASSERT_THAT(result, testing::Eq(3));
}

TEST(BreadthFirstSearchTests, BFSConnectivity_ShouldFind3PiecesInGivenGraph)
{
    auto result = graphs::BFSConnectivity(unconnectedGraph);

    ASSERT_THAT(result, testing::Eq(3));
}

TEST(BreadthFirstSearchTests, BFSConnectivity_ShouldFind1PieceInGivenGraph)
{
    auto result = graphs::BFSConnectivity(inputGraph);

    ASSERT_THAT(result, testing::Eq(1));
}


}