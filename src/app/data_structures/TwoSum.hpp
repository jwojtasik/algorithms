#pragma once

#include <unordered_set>
#include <set>

namespace data_structures
{
int twoSum(std::unordered_multiset<long long>& us, std::pair<int, int> sumRange);
int twoSumSecondApproach(std::set<long long>& us, std::pair<int, int> sumRange);
}