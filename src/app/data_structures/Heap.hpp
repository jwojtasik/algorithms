#pragma once

#include <vector>

namespace data_structures
{
class Heap
{
public:
    int top();
    void pop();
    void push(int num);
    size_t size();
    bool empty();

private:
    void bubbleUp(int index);
    void bubbleDown();

    bool comparator(int a, int b);
    void swap(size_t firstIndex, size_t secondIndex);
    std::vector<int> data;
};
}