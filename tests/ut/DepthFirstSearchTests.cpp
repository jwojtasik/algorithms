#include <gmock/gmock.h>
#include <list>
#include <map>

#include "app/graphs/DepthFirstSearch.hpp"

namespace
{
//    2 -- 4
//  /       \
// 1      -   6
//  \   /    /
//    3 -- 5
std::map<int, std::list<int>> inputGraph = {
    {1, {2, 3}}, {2, {1, 4}}, {3, {1, 5, 6}}, {4, {2, 6}}, {5, {3, 6}}, {6, {4, 5}}};
}
TEST(DepthFirstSearchTests, DFSTraverse_SouldVisitAllNodes)
{
    auto result = graphs::DFSTraverseGraph(inputGraph);

    ASSERT_THAT(result.size(), testing::Eq(inputGraph.size()));
}