#pragma once

#include <vector>

namespace divide_and_conquer
{
std::pair<std::vector<int>, long long> inversionCount(const std::vector<int>& first);
}