#include <gmock/gmock.h>
#include <unordered_set>

#include "app/data_structures/TwoSum.hpp"


namespace tests
{
TEST(TwoSumTests, ShouldFindTwoPairs)
{
    std::unordered_multiset<long long> us{1,2,3,4,5,6};

    auto res = data_structures::twoSum(us, {7,8});
    ASSERT_THAT(res, testing::Eq(2));
}

TEST(TwoSumTests, ShouldFindEightUniqePairsForGivenSet)
{
    std::unordered_multiset<long long> us{-3, -1, 1, 2, 9, 11, 7, 6, 2};
    auto sumRange = std::pair{3, 10};

    auto res = data_structures::twoSum(us, sumRange);
    ASSERT_THAT(res, testing::Eq(8));
}

TEST(TwoSumTests, ShouldFindFivePairsForGivenSet)
{
    std::unordered_multiset<long long> us{1, 2, 9, 2};
    auto sumRange = std::pair{3, 10};

    auto res = data_structures::twoSum(us, sumRange);
    ASSERT_THAT(res, testing::Eq(3));
}
}