#pragma once

#include <vector>

namespace greedy
{
long long schedulingSub(std::vector<std::pair<int, int>>& vec);
long long schedulingRatio(std::vector<std::pair<int, int>>& vec);
}