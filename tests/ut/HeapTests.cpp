#include "app/data_structures/Heap.hpp"
#include "gmock/gmock.h"

namespace tests
{
TEST(HeapTests, ShoulCorretlyInsertOneNumber)
{
    data_structures::Heap heap;
    heap.push(5);
    ASSERT_THAT(heap.top(), testing::Eq(5));
}

TEST(HeapTests, ShoulCorretlyInsertSecondNumberAsLeaf)
{
    data_structures::Heap heap;
    heap.push(5);
    heap.push(8);
    ASSERT_THAT(heap.top(), testing::Eq(5));
    ASSERT_THAT(heap.size(), testing::Eq(2));
}

TEST(HeapTests, ShoulCorretlyInsertSecondNumberAndChangeRoot)
{
    data_structures::Heap heap;
    heap.push(5);
    heap.push(3);
    ASSERT_THAT(heap.top(), testing::Eq(3));
    ASSERT_THAT(heap.size(), testing::Eq(2));
}

TEST(HeapTests, ShoulCorretlyInsertFourNambersAndChangeRootTwice)
{
    data_structures::Heap heap;
    heap.push(5);
    heap.push(3);
    ASSERT_THAT(heap.top(), testing::Eq(3));

    heap.push(10);
    heap.push(2);
    ASSERT_THAT(heap.top(), testing::Eq(2));
    ASSERT_THAT(heap.size(), testing::Eq(4));
}

TEST(HeapTests, AfterRootRemovalShouldRestoreHeapProperty)
{
    data_structures::Heap heap;
    heap.push(4);
    heap.push(4);
    heap.push(8);
    heap.push(9);
    heap.push(10);
    heap.push(12);
    heap.push(9);
    heap.push(11);
    heap.push(13);

    ASSERT_THAT(heap.top(), testing::Eq(4));
    ASSERT_THAT(heap.size(), testing::Eq(9));

    heap.pop();

    ASSERT_THAT(heap.top(), testing::Eq(4));
    ASSERT_THAT(heap.size(), testing::Eq(8));
}

TEST(HeapTests, ShouldBeEmptyAfterPushAndPop)
{
    data_structures::Heap heap;
    heap.push(4);
    ASSERT_THAT(heap.empty(), testing::Eq(false));

    heap.pop();

    ASSERT_THAT(heap.empty(), testing::Eq(true));
}

TEST(HeapTests, ShouldCorrectlyRestoreAfterTwoPushAndOnePop)
{
    data_structures::Heap heap;
    heap.push(4);
    heap.push(5);
    ASSERT_THAT(heap.empty(), testing::Eq(false));

    heap.pop();

    ASSERT_THAT(heap.size(), testing::Eq(1));
}
}