#include <gmock/gmock.h>

#include <vector>
#include "app/data_structures/MedianMaintenance.hpp"

namespace tests
{
TEST(MedianMaintenanceTests, ShouldCorrectlyReturnMedian)
{
    const auto numbers = std::vector<int>{5,8,2,9,48,1,34,7};

    auto x = data_structures::medianOnHeaps(numbers);

    ASSERT_THAT(x, testing::Eq(7.5));
}
}