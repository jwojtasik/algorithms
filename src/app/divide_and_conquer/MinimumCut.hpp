#include <map>
#include <list>

namespace divide_and_conquer
{
int minimumCut(std::map<int, std::list<int>> graph);
}