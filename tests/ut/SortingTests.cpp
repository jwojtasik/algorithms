#include <gmock/gmock.h>

#include <fstream>
#include <vector>

#include "app/divide_and_conquer/Sorting.hpp"

using namespace testing;
namespace tests
{
TEST(SortingTests, MergeSort_ShouldSortInvertedArrayWithEvenSize)
{
    auto result = divide_and_conquer::mergeSort(std::vector<int>{8, 7, 6, 5, 4, 3, 2, 1});

    ASSERT_EQ(result, result);
}

TEST(SortingTests, MergeSort_ShouldSortInvertedArrayWithOddSize)
{
    auto result = divide_and_conquer::mergeSort(std::vector<int>{3, 2, 1});

    ASSERT_EQ(result, result);
}

TEST(SortingTests, QuickSort_ShouldSortVectorOf3Intigers)
{
    auto vec = std::vector<int>{45, 70, 30};
    divide_and_conquer::quickSort(vec, 0, vec.size()-1);

    for (int i = 0; i < vec.size() - 1; ++i)
    {
        ASSERT_THAT(vec[i], testing::Le(vec[i + 1]));
    }
}

TEST(SortingTests, QuickSort_ShouldSortVectorOf6ReversedIntigers)
{
    auto vec = std::vector<int>{70, 60, 50, 40, 30, 20};
    divide_and_conquer::quickSort(vec, 0, vec.size()-1);

    for (int i = 0; i < vec.size() - 1; ++i)
    {
        ASSERT_THAT(vec[i], testing::Le(vec[i + 1]));
    }
}

TEST(SortingTests, QuickSort_ShouldSortVectorOf4Intigers)
{
    auto vec = std::vector<int>{50, 60, 30, 70};
    divide_and_conquer::quickSort(vec, 0, vec.size()-1);

    for (int i = 0; i < vec.size() - 1; ++i)
    {
        ASSERT_THAT(vec[i], testing::Le(vec[i + 1]));
    }
}

TEST(SortingTests, QuickSort_TestComparisonsOn10Numbers)
{
    auto vec = std::vector<int>{2148, 9058, 7742, 3153, 6324, 609 ,7628 ,5469 ,7017 ,504};
    auto m = divide_and_conquer::quickSort(vec, 0, vec.size()-1);

    for (int i = 0; i < vec.size() - 1; ++i)
    {
        ASSERT_THAT(vec[i], testing::Le(vec[i + 1]));
    }
}

TEST(SortingTests, DISABLED_QuickSort_ShouldSort10kSizeArray)
{
    std::vector<int> input;
    std::filebuf fb;
    if (fb.open("QuickSort.txt", std::ios::in))
    {
        std::istream is(&fb);
        std::copy(std::istream_iterator<int>(is), std::istream_iterator<int>(), std::back_inserter(input));
        fb.close();
    }

    auto m  = divide_and_conquer::quickSort(input, 0, input.size()-1);
    for (int i = 0; i < input.size() - 1; ++i)
    {
        ASSERT_THAT(input[i], testing::Le(input[i + 1]));
    }
}

TEST(SortingTests, RandomizedSelectiont_ShouldCorrectlyFindNthElement)
{
    auto vec = std::vector<int>{10, 8, 2, 4};
    auto x = divide_and_conquer::randomizedSelection(vec, 3);

    ASSERT_THAT(x, testing::Eq(8));
}

TEST(SortingTests, RandomizedSelectiont_ShouldCorrectlyFindNthElement2)
{
    auto vec = std::vector<int>{10, 8, 2, 4, 15, 60, 56, 12};
    auto x = divide_and_conquer::randomizedSelection(vec, 2);

    ASSERT_THAT(x, testing::Eq(4));
}

TEST(SortingTests, RandomizedSelectiont_ShouldCorrectlyFindNthElement3)
{
    auto vec = std::vector<int>{10, 8, 2, 4, 15, 60, 56, 12};
    auto x = divide_and_conquer::randomizedSelection(vec, 5);

    ASSERT_THAT(x, testing::Eq(12));
}

}