#pragma once

#include <list>
#include <map>

namespace graphs
{
int BFSShortestPath(std::map<int, std::list<int>>& graph, int from, int to);
int BFSConnectivity(std::map<int, std::list<int>>& graph);
}