#include "app/data_structures/Heap.hpp"

namespace data_structures
{
int Heap::top()
{
    return data.at(0);
}

void Heap::pop()
{
    swap(0, data.size() - 1);
    data.pop_back();

    if (!empty())
    {
        bubbleDown();
    }
}

size_t Heap::size()
{
    return data.size();
}

bool Heap::empty()
{
    return data.empty();
}

void Heap::push(int num)
{
    const auto insertedNode = data.emplace_back(num);
    const auto insertedIndex = data.size() - 1;
    if (size() == 1)
        return;
    bubbleUp(insertedIndex);
}

void Heap::bubbleUp(int index)
{
    auto parentIndex = index % 2 ? (index - 1) / 2 : floor((index - 1) / 2.0);
    auto parent = data.at(parentIndex);
    auto node = data.at(index);

    while (!comparator(parent, node))
    {
        swap(parentIndex, index);

        index = parentIndex;
        parentIndex = index % 2 ? floor(index / 2.0) : index / 2;
        parent = data.at(parentIndex);
        node = data.at(index);
    }
}

void Heap::bubbleDown()
{
    auto parent = top();
    auto parentIndex = 0;

    auto leftIndex = 2 * parentIndex + 1;
    auto rightIndex = 2 * parentIndex + 2;

    if (leftIndex >= size())
    {
        return;
    }
    auto leftChild = data.at(leftIndex);
    if (rightIndex >= size())
    {
        rightIndex = leftIndex;
    }
    int rightChild = data.at(rightIndex);
    while (!comparator(parent, leftChild) || !comparator(parent, rightChild))
    {
        if (comparator(leftChild, rightChild))
        {
            swap(parentIndex, leftIndex);
            parentIndex = leftIndex;
        }
        else
        {
            swap(parentIndex, rightIndex);
            parentIndex = rightIndex;
        }

        parent = data.at(parentIndex);
        leftIndex = 2 * parentIndex + 1;
        rightIndex = 2 * parentIndex + 2;
        if (leftIndex >= size())
        {
            return;
        }
        leftChild = data.at(leftIndex);
        if (rightIndex >= size())
        {
            rightChild = data.at(leftIndex);
        }
        else
        {
            rightChild = data.at(rightIndex);
        }
    }
}

void Heap::swap(size_t firstIndex, size_t secondIndex)
{
    const auto tmp = data.at(firstIndex);
    data.at(firstIndex) = data.at(secondIndex);
    data.at(secondIndex) = tmp;
}

bool Heap::comparator(int first, int second)
{
    return first <= second;
}
}