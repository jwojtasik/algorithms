#include <gtest/gtest.h>

#include "app/divide_and_conquer/Multiplier.hpp"

using namespace testing;
namespace tests
{
TEST(Test, MultiplySchoolEasy)
{
    auto x = "7006652";
    auto multi = divide_and_conquer::Multiplier("5678", "1234");

    ASSERT_EQ(x, multi.school());
}

TEST(Test, MultiplySchoolTask)
{
    std::string x =
        "85397342226735670654635508695465744950348885357651149618796011270677430448932048486178750722162490730133748958"
        "71952806582723184";
    auto multi = divide_and_conquer::Multiplier("3141592653589793238462643383279502884197169399375105820974944592",
        "2718281828459045235360287471352662497757247093699959574966967627");
    ASSERT_EQ(x, multi.school());
}

TEST(Test, MultiplyRecursiveEasy)
{
    auto x = "726";
    auto multi = divide_and_conquer::Multiplier("33", "22");

   ASSERT_EQ(x, multi.recursive());
}

TEST(Test, MultiplyRecursiveEasy2)
{
    auto x = "7006652";
    auto multi = divide_and_conquer::Multiplier("5678", "1234");

    ASSERT_EQ(x, multi.recursive());
}

TEST(Test, MultiplyRecursiveTask)
{
    std::string x =
        "85397342226735670654635508695465744950348885357651149618796011270677430448932048486178750722162490730133748958"
        "71952806582723184";
    auto multi = divide_and_conquer::Multiplier("3141592653589793238462643383279502884197169399375105820974944592",
        "2718281828459045235360287471352662497757247093699959574966967627");
    ASSERT_EQ(x, multi.recursive());
}
}