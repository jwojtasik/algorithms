#include "app/data_structures/MedianMaintenance.hpp"

#include <queue>
#include "app/data_structures/Heap.hpp"

namespace data_structures
{
float medianOnHeaps(const std::vector<int>& numbers)
{
    std::priority_queue<int> maxOnTop;
    //data_structures::Heap minOnTop;
    std::priority_queue<int, std::vector<int>, std::greater<int>> minOnTop;

    for (const auto& num : numbers)
    {
        if (maxOnTop.size() == minOnTop.size())
        {
            maxOnTop.push(num);
        }
        else
        {
            minOnTop.push(num);
        }

        if ((!minOnTop.empty() && !maxOnTop.empty())  && minOnTop.top() < maxOnTop.top())
        {
            auto minTop = maxOnTop.top();
            auto maxTop = minOnTop.top();
            maxOnTop.pop();
            minOnTop.pop();
            maxOnTop.push(maxTop);
            minOnTop.push(minTop);
        }
    }

    return minOnTop.size() == maxOnTop.size() ? (minOnTop.top() + maxOnTop.top()) / 2.0 : maxOnTop.top();
}
}